@extends('layout.master')

@section('judul')
Tambah Peran
@endsection
    
@section('content')
    <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label >nama pemain</label>
                    <input type="text" class="form-control" name="nama"  placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >umur</label>
                    <input type="text" class="form-control" name="umur" placeholder="Masukkan umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >bio</label>
                    <input type="text" class="form-control" name="bio" placeholder="Masukkan bio">
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
    </form>


@endsection