@extends('layout.master')

@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
@csrf
@method('PUT') 
            <div class="form-group">
                <label >nama pemain</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama"  placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Body">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label >bio</label>
                <input type="text" class="form-control" value="{{$cast->bio}}" name="bio" placeholder="Masukkan Body">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection