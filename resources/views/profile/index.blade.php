@extends('layout.master')

@section('judul')
Edit Profile
@endsection
    
@section('content')
    <form action="/profile/{{$profile->id}}" method="POST">
                @csrf
                @method('put')
                
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
                </div>                
                <div class="form-group">
                    <label >Email</label>
                    <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
                </div>
                
                
                <div class="form-group">
                    <label >Umur</label>
                    <input type="number" class="form-control" name="umur" value="{{$profile->umur}}">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Biodata</label>
                    <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label >Alamat</label>
                    <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
                    @error('alamat')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
    </form>


@endsection

