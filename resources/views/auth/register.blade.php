<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register Account</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('Admin/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('Admin/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('Admin/assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('Admin//assets/css/style.css')}}">
    <!-- End layout styles -->
    
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="row justify-content-center">
                  <h4>Register Now</h4>
                </div>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href=""><b>Gudang</b>Film</a>
  </div>

                  <div class="form-group">
                    <input type="email" class="form-control form-control-lg" name="email" placeholder="Email">
                  </div>
                  @error('email')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="form-group">
                    <input type="password" class="form-control form-control-lg" name="password" placeholder="Password">
                  </div>
                  @error('password')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="form-group">
                    <input type="password" class="form-control form-control-lg" name="password_confirmation" placeholder="ConfirmPassword">
                  </div>
                  @error('password_confirmation')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="form-group">
                    <input type="number" class="form-control form-control-lg" name="umur" placeholder="isi umur anda" >
                  </div>
                  @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="form-group">
                    <textarea type="text" class="form-control form-control-lg" name="bio" placeholder="isi bio anda"></textarea>
                  </div>
                  @error('bio')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="form-group">
                    <textarea type="text" class="form-control form-control-lg" name="alamat" placeholder="isi alamat anda"></textarea>
                  </div>
                  @error('alamat')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
                  @enderror

                  <div class="mt-3">
                    <input type="submit" class="btn btn-block btn-primary"></a>
                  </div>
                  <div class="text-center mt-4 font-weight-light"> Already have an account? <a href="/login" class="text-primary">Login</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
            <input type="number" class="form-control" name="umur" placeholder="Masukan Umur">            
         </div>
  
        @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror

        <div class="input-group mb-3">
        <textarea name="bio" id="" class="form-control" placeholder="Masukan Bio"></textarea>            
        </div>

        @error('bio')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror

        <div class="input-group mb-3">
            <textarea name="alamat" id="" class="form-control" placeholder="Masukan Alamat"></textarea>            
            </div>
    
            @error('alamat')
            <div class="alert alert-danger">
              {{ $message }}
            </div>
            @enderror

        <div class="row">
          <!-- /.col -->
            <button type="submit" class="btn btn-primary btn-block">Register</button>          
          <!-- /.col -->
        </div>
      </form>
      
      <a href="/login" class="text-center">I already have a membership</a>
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('Admin//assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('Admin/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('Admin/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('Admin//assets/js/misc.js')}}"></script>
    <!-- endinject -->
  </body>
</html>