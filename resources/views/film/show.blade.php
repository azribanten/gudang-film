@extends('layout.master')

@section('judul')
Detail Film id ke {{$film->id}}
@endsection

@section('content')

<img src="{{asset('poster/'. $film->poster)}}" alt="">
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>

<h2>Kritik</h2>

@forelse ($film->kritik as $item)
    <div class="card">
        <div class="card-body">
            <small><b>{{$item->user->name}}</b></small>
            <p class="card-text">{{$item->isi}}</p>
            <p class="card-text">Rating: {{$item->point}}</p>
        </div>
    </div> 

@empty
    <h4> Tidak Ada Komentar </h4>    
@endforelse



<form action="/kritik" class="my-2" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="form-group">
        <label >Isi Kritik</label>
        <input type="hidden" value="{{$film->id}}" name="film_id">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
        @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label >Rating</label>
        <input type="number"  class="form-control" name="point"  placeholder="Masukkan Title">
        @error('point')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Back</a>

@endsection