@extends('layout.master')

@section('judul')
Edit Genre
@endsection
    
@section('content')
    <form action="/film/{{$genre->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label >Genre Film</label>
                    <input type="text" class="form-control" value="{{$genre->genrenya}}" name="genrenya"  placeholder="Masukkan Title">
                    @error('genrenya')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                
                
                <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection