<ul class="nav">
  <li class="nav-item nav-category">Main</li>
  <li class="nav-item">
    <a class="nav-link" href="index.html">
      <span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
      <span class="menu-title">Dashboard</span>
    </a>
    
  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
      <span class="icon-bg"><i class="mdi mdi-crosshairs-gps menu-icon"></i></span>
      <span class="menu-title">UI Elements</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="ui-basic">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item"> <a class="nav-link" href="/film">Film</a></li>
        <li class="nav-item"> <a class="nav-link" href="/cast">Cast</a></li> 
        <li class="nav-item"> <a class="nav-link" href="/genre">genre</a></li> 
  </li>
  
</ul>