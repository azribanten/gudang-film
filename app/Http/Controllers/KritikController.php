<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kritik;  
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
        public function store(Request $request){
            $request->validate([
                'isi' => 'required',
                'point' => 'required'
            ]);
            
            $kritik = new Kritik;

            $kritik->isi = $request->isi;
            $kritik->point = $request->point;
            $kritik->user_id = Auth::id();
            $kritik->film_id = $request->film_id;

            $kritik->save();

            return redirect()->back();
        }
}
